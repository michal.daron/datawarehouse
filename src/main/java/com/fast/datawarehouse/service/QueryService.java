package com.fast.datawarehouse.service;

import com.fast.datawarehouse.dto.request.QueryRequestDTO;
import com.fast.datawarehouse.dto.response.QueryResponseDTO;
import com.fast.datawarehouse.model.DataWarehouse;

import javax.xml.bind.ValidationException;
import java.util.List;

public interface QueryService {

    boolean createEntry(DataWarehouse model);

    boolean createEntry(String dataSource, String campaign, String date, long clicks, long impressions);

    List<DataWarehouse> retrieveAllForSourceAndCampaign(String source, String campaign);

    QueryResponseDTO query(QueryRequestDTO request) throws ValidationException;

    boolean populateDataBaseFromCSV() throws ValidationException;
}
