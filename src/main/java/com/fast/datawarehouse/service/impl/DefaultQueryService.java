package com.fast.datawarehouse.service.impl;

import com.fast.datawarehouse.dto.request.QueryRequestDTO;
import com.fast.datawarehouse.dto.response.QueryResponseDTO;
import com.fast.datawarehouse.model.DataWarehouse;
import com.fast.datawarehouse.repository.DataWarehouseRepository;
import com.fast.datawarehouse.service.QueryService;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.xml.bind.ValidationException;
import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Service
public class DefaultQueryService implements QueryService {

    @Autowired
    private DataWarehouseRepository repository;

    private static final String FILE_PATH = "./PIxSyyrIKFORrCXfMYqZBI.csv";

    @Override
    public boolean createEntry(DataWarehouse model) {

        this.repository.save(model);

        return true;
    }

    @Override
    public boolean createEntry(String dataSource, String campaign, String stringDate, long clicks, long impressions) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yy");
        LocalDate date = LocalDate.parse(stringDate, formatter);
        return this.createEntry(new DataWarehouse(dataSource, campaign, date, clicks, impressions));
    }

    @Override
    public List<DataWarehouse> retrieveAllForSourceAndCampaign(String source, String campaign) {
        return this.repository.findAllByDataSourceAndCampaign(source, campaign);
    }

    @Override
    public QueryResponseDTO query(QueryRequestDTO request) throws ValidationException {

        validateQueryRequest(request);

        QueryResponseDTO responseDTO = new QueryResponseDTO();
        responseDTO.setMetricType(request.getMetricType());
        responseDTO.setQueryPerDay(request.isQueryPerDay());
        responseDTO.setDataSource(request.getDataSource());
        responseDTO.setCampaign(request.getCampaign());
        responseDTO.setStartDate(request.getStartDate());
        responseDTO.setEndDate(request.getEndDate());

        if (request.isQueryPerDay()) {
            responseDTO.setPerDayResult(getTotalPerDay(request));
        } else {
            responseDTO.setTotalResult(getTotal(request));
        }

        return responseDTO;
    }

    @Override
    public boolean populateDataBaseFromCSV() throws ValidationException {

        Reader reader;
        try {
            reader = Files.newBufferedReader(Paths.get(FILE_PATH));
        } catch (IOException e) {
            throw new ValidationException("Failed to read CSV file.");
        }
        CsvToBean<DataWarehouse> csvToBean = new CsvToBeanBuilder<DataWarehouse>(reader)
                .withType(DataWarehouse.class)
                .withSkipLines(1)
                .withIgnoreLeadingWhiteSpace(true)
                .build();

        List<DataWarehouse> dwList = csvToBean.parse();

        dwList.parallelStream().forEach(this::createEntry);

        return true;
    }

    private Map<LocalDate, String> getTotalPerDay(QueryRequestDTO requestDTO) {

        TreeMap<LocalDate, String> result = new TreeMap<>();

        List<DataWarehouse> resultList;

        if (!StringUtils.isEmpty(requestDTO.getDataSource()) && !StringUtils.isEmpty(requestDTO.getCampaign())) {
            if (requestDTO.getStartDate() != null && requestDTO.getEndDate() != null) {
                resultList = this.repository.findAllByDataSourceAndCampaignAndDateBetween
                        (requestDTO.getDataSource(), requestDTO.getCampaign(), requestDTO.getStartDate(), requestDTO.getEndDate());
            } else {
                resultList = this.repository.findAllByDataSourceAndCampaign(requestDTO.getDataSource(), requestDTO.getCampaign());
            }
        } else {
            if (requestDTO.getStartDate() != null && requestDTO.getEndDate() != null) {
                resultList = this.repository.findAllByDateBetween(requestDTO.getStartDate(), requestDTO.getEndDate());
            } else {
                resultList = this.repository.findAll();
            }
        }

        if (resultList == null || resultList.isEmpty()) {
            return null;
        }

        switch (requestDTO.getMetricType()) {
            case CLICKS:
                Map<LocalDate, Long> totalClicks = new HashMap<>();
                resultList.forEach(dw -> totalClicks.merge(dw.getDate(), dw.getClicks(), Long::sum));
                totalClicks.forEach((k, v) -> result.put(k, v.toString()));
                break;
            case IMPRESSIONS:
                Map<LocalDate, Long> totalImpr = new HashMap<>();
                resultList.forEach(dw -> totalImpr.merge(dw.getDate(), dw.getImpressions(), Long::sum));
                totalImpr.forEach((k, v) -> result.put(k, v.toString()));
                break;
            case CTR:
                Map<LocalDate, Long> clicks = new HashMap<>();
                resultList.forEach(dw -> clicks.merge(dw.getDate(), dw.getClicks(), Long::sum));

                Map<LocalDate, Long> impressions = new HashMap<>();
                resultList.forEach(dw -> impressions.merge(dw.getDate(), dw.getImpressions(), Long::sum));

                clicks.forEach((k, v) -> {
                    if (impressions.get(k) != null && impressions.get(k) > 0) {
                        result.put(k, new DecimalFormat("#.##").format(((Double.valueOf(v) / Double.valueOf(impressions.get(k))) * 100)));
                    }
                });
                break;
        }

        return result;
    }

    private String getTotal(QueryRequestDTO requestDTO) {

        if (!StringUtils.isEmpty(requestDTO.getDataSource()) && !StringUtils.isEmpty(requestDTO.getCampaign())) {
            if (requestDTO.getStartDate() != null && requestDTO.getEndDate() != null) {
                switch (requestDTO.getMetricType()) {
                    case CLICKS:
                        return this.repository.countClicks
                                (requestDTO.getDataSource(), requestDTO.getCampaign(), requestDTO.getStartDate(), requestDTO.getEndDate()).toString();

                    case IMPRESSIONS:
                        return this.repository.countImpressions
                                (requestDTO.getDataSource(), requestDTO.getCampaign(), requestDTO.getStartDate(), requestDTO.getEndDate()).toString();

                    case CTR:
                        Long clicks = this.repository.countClicks(requestDTO.getDataSource(), requestDTO.getCampaign(), requestDTO.getStartDate(), requestDTO.getEndDate());
                        Long impr = this.repository.countImpressions(requestDTO.getDataSource(), requestDTO.getCampaign(), requestDTO.getStartDate(), requestDTO.getEndDate());
                        return new DecimalFormat("#.##").format(((Double.valueOf(clicks) / Double.valueOf(impr)) * 100));
                }
            } else {
                switch (requestDTO.getMetricType()) {
                    case CLICKS:
                        return this.repository.countClicks(requestDTO.getDataSource(), requestDTO.getCampaign()).toString();
                    case IMPRESSIONS:
                        return this.repository.countImpressions(requestDTO.getDataSource(), requestDTO.getCampaign()).toString();
                    case CTR:
                        Long clicks = this.repository.countClicks(requestDTO.getDataSource(), requestDTO.getCampaign());
                        Long impr = this.repository.countImpressions(requestDTO.getDataSource(), requestDTO.getCampaign());
                        return new DecimalFormat("#.##").format(((Double.valueOf(clicks) / Double.valueOf(impr)) * 100));
                }
            }
        } else {
            if (requestDTO.getStartDate() != null && requestDTO.getEndDate() != null) {
                switch (requestDTO.getMetricType()) {
                    case CLICKS:
                        return this.repository.countClicks(requestDTO.getStartDate(), requestDTO.getEndDate()).toString();

                    case IMPRESSIONS:
                        return this.repository.countImpressions(requestDTO.getStartDate(), requestDTO.getEndDate()).toString();

                    case CTR:
                        Long clicks = this.repository.countClicks(requestDTO.getDataSource(), requestDTO.getCampaign(), requestDTO.getStartDate(), requestDTO.getEndDate());
                        Long impr = this.repository.countImpressions(requestDTO.getDataSource(), requestDTO.getCampaign(), requestDTO.getStartDate(), requestDTO.getEndDate());
                        return new DecimalFormat("#.##").format(((Double.valueOf(clicks) / Double.valueOf(impr)) * 100));
                }
            } else {
                switch (requestDTO.getMetricType()) {
                    case CLICKS:
                        return this.repository.countClicks().toString();
                    case IMPRESSIONS:
                        return this.repository.countImpressions().toString();
                    case CTR:
                        Long clicks = this.repository.countClicks();
                        Long impr = this.repository.countImpressions();
                        return new DecimalFormat("#.##").format(((Double.valueOf(clicks) / Double.valueOf(impr)) * 100));
                }
            }
        }

        return null;
    }

    private void validateQueryRequest(QueryRequestDTO requestDTO) throws ValidationException {

        if (requestDTO.getMetricType() == null) {
            throw new ValidationException("Please provide metric type.");
        }

        if ((StringUtils.isEmpty(requestDTO.getDataSource()) && !StringUtils.isEmpty(requestDTO.getCampaign()))
                || (!StringUtils.isEmpty(requestDTO.getDataSource()) && StringUtils.isEmpty(requestDTO.getCampaign()))) {
            throw new ValidationException("Please provide both data source and campaign or none at all.");
        }

        if ((requestDTO.getStartDate() == null && requestDTO.getEndDate() != null)
                || requestDTO.getStartDate() != null && requestDTO.getEndDate() == null) {
            throw new ValidationException("Please provide both start date and end date or none at all.");
        }
    }
}
