package com.fast.datawarehouse.repository;

import com.fast.datawarehouse.model.DataWarehouse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface DataWarehouseRepository extends JpaRepository<DataWarehouse, Long> {

    List<DataWarehouse> findAllByDataSourceAndCampaign(String dataSource, String campaign);

    List<DataWarehouse> findAllByDataSourceAndCampaignAndDateBetween(String dataSource, String campaign, LocalDate start, LocalDate end);

    List<DataWarehouse> findAllByDateBetween(LocalDate start, LocalDate end);

    List<DataWarehouse> findAll();

    @Query("SELECT SUM(d.clicks) FROM DataWarehouse d WHERE d.campaign = :#{#campaign} and d.dataSource = :#{#dataSource}")
    Long countClicks(@Param("dataSource") String dataSource, @Param("campaign") String campaign);

    @Query("SELECT SUM(d.clicks) FROM DataWarehouse d")
    Long countClicks();

    @Query("SELECT SUM(d.impressions) FROM DataWarehouse d WHERE d.campaign = :#{#campaign} and d.dataSource = :#{#dataSource}")
    Long countImpressions(@Param("dataSource") String dataSource, @Param("campaign") String campaign);

    @Query("SELECT SUM(d.impressions) FROM DataWarehouse d")
    Long countImpressions();

    @Query("SELECT SUM(d.clicks) FROM DataWarehouse d WHERE d.campaign = :#{#campaign} and d.dataSource = :#{#dataSource} and d.date BETWEEN :#{#start} and :#{#end}")
    Long countClicks(@Param("dataSource") String dataSource, @Param("campaign") String campaign, @Param("start") LocalDate start, @Param("end") LocalDate end);

    @Query("SELECT SUM(d.clicks) FROM DataWarehouse d WHERE d.date BETWEEN :#{#start} and :#{#end}")
    Long countClicks(@Param("start") LocalDate start, @Param("end") LocalDate end);

    @Query("SELECT SUM(d.impressions) FROM DataWarehouse d WHERE d.campaign = :#{#campaign} and d.dataSource = :#{#dataSource} and d.date BETWEEN :#{#start} and :#{#end}")
    Long countImpressions(@Param("dataSource") String dataSource, @Param("campaign") String campaign, @Param("start") LocalDate start, @Param("end") LocalDate end);

    @Query("SELECT SUM(d.impressions) FROM DataWarehouse d WHERE d.date BETWEEN :#{#start} and :#{#end}")
    Long countImpressions(@Param("start") LocalDate start, @Param("end") LocalDate end);
}
