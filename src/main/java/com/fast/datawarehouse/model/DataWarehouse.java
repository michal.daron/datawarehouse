package com.fast.datawarehouse.model;

import com.opencsv.bean.CsvBindByPosition;
import com.opencsv.bean.CsvDate;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Entity
public class DataWarehouse {

    public DataWarehouse(String dataSource, String campaign, LocalDate date, long clicks, long impressions) {
        this.dataSource = dataSource;
        this.campaign = campaign;
        this.date = date;
        this.clicks = clicks;
        this.impressions = impressions;
    }

    public DataWarehouse() {}

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @CsvBindByPosition(position = 0)
    @Column(name = "data_source")
    private String dataSource;

    @CsvBindByPosition(position = 1)
    @Column(name = "campaign")
    private String campaign;

    @CsvDate(value = "MM/dd/yy")
    @CsvBindByPosition(position = 2)
    @Column(name = "date")
    private LocalDate date;

    @CsvBindByPosition(position = 3)
    @Column(name = "clicks")
    private long clicks;

    @CsvBindByPosition(position = 4)
    @Column(name = "impressions")
    private long impressions;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDataSource() {
        return dataSource;
    }

    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    public String getCampaign() {
        return campaign;
    }

    public void setCampaign(String campaign) {
        this.campaign = campaign;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public long getClicks() {
        return clicks;
    }

    public void setClicks(long clicks) {
        this.clicks = clicks;
    }

    public long getImpressions() {
        return impressions;
    }

    public void setImpressions(long impressions) {
        this.impressions = impressions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DataWarehouse that = (DataWarehouse) o;
        return clicks == that.clicks &&
                impressions == that.impressions &&
                dataSource.equals(that.dataSource) &&
                campaign.equals(that.campaign) &&
                date.equals(that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dataSource, campaign, date, clicks, impressions);
    }
}
