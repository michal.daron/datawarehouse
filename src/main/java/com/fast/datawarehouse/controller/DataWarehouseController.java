package com.fast.datawarehouse.controller;

import com.fast.datawarehouse.dto.request.QueryRequestDTO;
import com.fast.datawarehouse.dto.response.QueryResponseDTO;
import com.fast.datawarehouse.model.DataWarehouse;
import com.fast.datawarehouse.service.QueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.xml.bind.ValidationException;

@RestController
@RequestMapping(
        value = "/query",
        produces = {MediaType.APPLICATION_JSON_VALUE}
)
public class DataWarehouseController {

    @Autowired
    private QueryService queryService;

    @PostMapping(path = "/addOne")
    public @ResponseBody
    boolean createEntry(@RequestParam String dataSource,
                        @RequestParam String campaign,
                        @RequestParam String date,
                        @RequestParam long clicks,
                        @RequestParam long impressions) {

        return queryService.createEntry(dataSource, campaign, date, clicks, impressions);
    }

    @GetMapping(path = "/get")
    public @ResponseBody Iterable<DataWarehouse> retrieveAllForSourceAndCampaign(@RequestParam String dataSource, @RequestParam String campaign) {
        return this.queryService.retrieveAllForSourceAndCampaign(dataSource, campaign);
    }

    @PostMapping(path = "/queryData")
    public @ResponseBody
    QueryResponseDTO queryData(@RequestBody QueryRequestDTO requestDTO) throws ValidationException {
        return this.queryService.query(requestDTO);
    }

    //TODO use this only to insert data in DB from a csv file
//    @PostMapping(path = "/populateDB")
//    public @ResponseBody
//    Boolean populateDatabase() throws ValidationException {
//        return this.queryService.populateDataBaseFromCSV();
//    }
}
