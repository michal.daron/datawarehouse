package com.fast.datawarehouse.enums;

public enum MetricType {
    CLICKS,
    IMPRESSIONS,
    CTR
}
