package com.fast.datawarehouse.dto.request;

import com.fast.datawarehouse.enums.MetricType;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.lang.NonNull;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

@ApiModel
public class QueryRequestDTO {

    @ApiModelProperty
    @NonNull
    private MetricType metricType;

    @ApiModelProperty
    private boolean queryPerDay = false;

    @ApiModelProperty
    private String dataSource;

    @ApiModelProperty
    private String campaign;

    @ApiModelProperty
    @JsonFormat(pattern = "MM/dd/yy", shape = JsonFormat.Shape.STRING)
    private LocalDate startDate;

    @ApiModelProperty
    @JsonFormat(pattern = "MM/dd/yy", shape = JsonFormat.Shape.STRING)
    private LocalDate endDate;

    public QueryRequestDTO(MetricType metricType, boolean queryPerDay, String dataSource, String campaign, LocalDate startDate, LocalDate endDate) {
        this.metricType = metricType;
        this.queryPerDay = queryPerDay;
        this.dataSource = dataSource;
        this.campaign = campaign;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public QueryRequestDTO() {}

    public MetricType getMetricType() {
        return metricType;
    }

    public boolean isQueryPerDay() {
        return queryPerDay;
    }

    public String getDataSource() {
        return dataSource;
    }

    public String getCampaign() {
        return campaign;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QueryRequestDTO that = (QueryRequestDTO) o;
        return queryPerDay == that.queryPerDay &&
                metricType == that.metricType &&
                Objects.equals(dataSource, that.dataSource) &&
                Objects.equals(campaign, that.campaign) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(metricType, queryPerDay, dataSource, campaign, startDate, endDate);
    }
}
