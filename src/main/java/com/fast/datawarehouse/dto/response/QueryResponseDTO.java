package com.fast.datawarehouse.dto.response;

import com.fast.datawarehouse.enums.MetricType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDate;
import java.util.Map;
import java.util.Objects;

@ApiModel
public class QueryResponseDTO {

    @ApiModelProperty
    private MetricType metricType;

    @ApiModelProperty
    private boolean queryPerDay = false;

    @ApiModelProperty
    private String dataSource;

    @ApiModelProperty
    private String campaign;

    @ApiModelProperty
    private LocalDate startDate;

    @ApiModelProperty
    private LocalDate endDate;

    @ApiModelProperty
    private String totalResult;

    @ApiModelProperty
    private Map<LocalDate, String> perDayResult;

    public QueryResponseDTO(MetricType metricType, boolean queryPerDay, String dataSource, String campaign, LocalDate startDate, LocalDate endDate,
                            String totalResult, Map<LocalDate, String> perDayResult) {
        this.metricType = metricType;
        this.queryPerDay = queryPerDay;
        this.dataSource = dataSource;
        this.campaign = campaign;
        this.startDate = startDate;
        this.endDate = endDate;
        this.totalResult = totalResult;
        this.perDayResult = perDayResult;
    }

    public QueryResponseDTO() {
    }

    public MetricType getMetricType() {
        return metricType;
    }

    public void setMetricType(MetricType metricType) {
        this.metricType = metricType;
    }

    public boolean isQueryPerDay() {
        return queryPerDay;
    }

    public void setQueryPerDay(boolean queryPerDay) {
        this.queryPerDay = queryPerDay;
    }

    public String getDataSource() {
        return dataSource;
    }

    public void setDataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    public String getCampaign() {
        return campaign;
    }

    public void setCampaign(String campaign) {
        this.campaign = campaign;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public Map<LocalDate, String> getPerDayResult() {
        return perDayResult;
    }

    public void setPerDayResult(Map<LocalDate, String> perDayResult) {
        this.perDayResult = perDayResult;
    }

    public String getTotalResult() {
        return totalResult;
    }

    public void setTotalResult(String totalResult) {
        this.totalResult = totalResult;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QueryResponseDTO that = (QueryResponseDTO) o;
        return queryPerDay == that.queryPerDay &&
                metricType == that.metricType &&
                Objects.equals(dataSource, that.dataSource) &&
                Objects.equals(campaign, that.campaign) &&
                Objects.equals(startDate, that.startDate) &&
                Objects.equals(endDate, that.endDate) &&
                Objects.equals(totalResult, that.totalResult) &&
                Objects.equals(perDayResult, that.perDayResult);
    }

    @Override
    public int hashCode() {
        return Objects.hash(metricType, queryPerDay, dataSource, campaign, startDate, endDate, totalResult, perDayResult);
    }
}
