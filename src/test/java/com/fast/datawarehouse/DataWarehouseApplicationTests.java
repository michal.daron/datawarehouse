package com.fast.datawarehouse;

import com.fast.datawarehouse.controller.DataWarehouseController;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DataWarehouseApplicationTests {

	@Autowired
	private DataWarehouseController controller;

	@Test
	void contextLoads() {
		Assertions.assertThat(controller).isNotNull();
	}

}
