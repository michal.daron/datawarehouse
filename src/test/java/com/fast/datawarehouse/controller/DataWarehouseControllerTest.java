package com.fast.datawarehouse.controller;

import com.fast.datawarehouse.dto.request.QueryRequestDTO;
import com.fast.datawarehouse.dto.response.QueryResponseDTO;
import com.fast.datawarehouse.enums.MetricType;
import com.fast.datawarehouse.service.impl.DefaultQueryService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(DataWarehouseController.class)
class DataWarehouseControllerTest {

    @Autowired
    private MockMvc mock;

    private QueryRequestDTO request;
    private Map<LocalDate, String> perDayResponse;
    private QueryResponseDTO response;

    @MockBean
    private DefaultQueryService service;

    @BeforeEach
    void setUp() {
        initMocks(this);

        request = new QueryRequestDTO(MetricType.CLICKS, true, "testDataSource", "testCampaign", LocalDate.now(), LocalDate.now());
        perDayResponse = new HashMap<>();
        response = new QueryResponseDTO(MetricType.CLICKS, false,
                "testDataSource", "testCampaign", LocalDate.now(), LocalDate.now(), "", perDayResponse);
    }

    @Test
    void queryStatusOK() throws Exception {
        mock.perform(post("/query/queryData")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(new QueryRequestDTO())))
                .andExpect(status().isOk());
    }

    @Test
    void query() throws Exception {
        response.setTotalResult("100");
        Mockito.when(service.query(Mockito.any(QueryRequestDTO.class))).thenReturn(response);

        MvcResult result = mock.perform(post("/query/queryData")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(new QueryRequestDTO())))
                .andExpect(status().isOk()).andReturn();

        assertThat(result).isNotNull();
        assertThat(result.getResponse().getContentAsString().contains("\"totalResult\":\"100\"")).isTrue();

    }

    private static String toJson(final Object obj) {
        try {
            return new ObjectMapper().writer().withDefaultPrettyPrinter().writeValueAsString(obj);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}
